# 访客预约管理系统

#### 介绍
本项目基于小诺开源框架二次开发 功能简单 会持续更新
访客可以通过系统登记 由讲师确认预约申请 管理员可查看所有预约记录已经增删改讲师

#### 软件架构
软件架构说明


#### 安装教程

您的开发电脑需要安装：NodeJs（14.x）、npm或yarn（最新版）建议使用yarn、Mysql5.7、Jdk1.8、Maven3.6.3（最新版）、开发工具推荐idea

启动前端：打开_web文件夹，进行依赖下载，运行npm install或yarn命令，再运行npm run serve或 yarn run serve
启动后端：打开application-local中配置数据库信息，运行SnowyApplication类即可启动
浏览器访问：http://localhost:81 （默认前端端口为：81，后端端口为：82）

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
